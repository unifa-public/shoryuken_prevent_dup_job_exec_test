#!/bin/sh
awslocal sqs create-queue --queue-name my-test-dlq
awslocal sqs create-queue --queue-name my-test \
  --attributes '{"RedrivePolicy": "{\"deadLetterTargetArn\":\"arn:aws:sqs:ap-northeast-1:000000000000:my-test-dlq\",\"maxReceiveCount\":3}"}'