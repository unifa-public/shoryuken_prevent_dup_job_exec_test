::Shoryuken.configure_server do |config|
  ::Shoryuken::Logging.logger = ::Logger.new(STDOUT, formatter: ::Logger::Formatter.new)

  config.options[:concurrency] = 2
  config.sqs_client_receive_message_opts = { wait_time_seconds: 20 }

  require 'shoryuken/middleware/server/lock'
  ::Shoryuken::Middleware::Server::Lock.redis_url = 'redis://localhost:6379/0'
  config.server_middleware do |chain|
    chain.insert_before(::Shoryuken::Middleware::Server::AutoDelete,
                        ::Shoryuken::Middleware::Server::Lock)
  end
end
