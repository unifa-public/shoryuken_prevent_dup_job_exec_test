# frozen_string_literal: true

::Aws.config.update(
  region: 'ap-northeast-1',
  credentials: ::Aws::Credentials.new('aws_access_key_id', 'aws_secret_access_key')
)
