class HelloWorldJob < ApplicationJob
  queue_as 'http://localhost:4566/000000000000/my-test'

  def perform(*_args)
    puts "Hello world!"
    logger.info("Hello world!")
  end
end
