class ErrorJob < ApplicationJob
  queue_as 'http://localhost:4566/000000000000/my-test'

  def perform(*_args)
    raise 'Error!'
  end
end
