module Shoryuken::Middleware::Server
  class Lock
    include ::Shoryuken::Util

    class << self
      attr_accessor :redis_url
    end

    def call(_worker, queue, sqs_msg, _body)
      redis = ::Redis.new(url: self.class.redis_url)

      # 本番用
      lock_key = "shoryuken_lock/#{queue}/#{sqs_msg.message_id}"
      # テスト用
      # lock_key = "shoryuken_lock/test"

      locked = redis.set(lock_key, 'lock', ex: 60, nx: true)
      unless locked
        logger.info("Cannot get lock. lock_key: #{lock_key}")
        return
      end
      logger.debug("Locked. lock_key: #{lock_key}")

      begin
        yield
      rescue ::StandardError => _e
        redis.del(lock_key)
        logger.debug("Unlocked. lock_key: #{lock_key}")
        raise
      else
        redis.set(lock_key, 'processed', ex: 60)
        logger.debug("Expand lock expiration. lock_key: #{lock_key}")
      end
    end
  end
end
