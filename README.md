# ShoryukenPreventDuplicatedJobExecutionTest

[Shoryuken](https://github.com/phstc/shoryuken) ではメッセージキューに[Amazon SQS]() を使用しています。
Amazon SQSの標準キューは、メッセージの重複配信が発生する可能性があります。
もしメッセージの重複配信が発生してしまうと、1つのジョブが複数回実行されてしまいます。

この問題の対策として、Shoryuken middlewareとRedisを使用した排他制御の仕組みを実装しました。
このリポジトリは、その挙動の確認用に作成しています。

## Requirements

* Ruby
* bundler
* docker & docker-compose

## Install

下記コマンドを実行してください。

```sh
# In your terminal
git clone https://bitbucket.org/unifa-public/shoryuken_prevent_dup_job_exec_test.git

cd shoryuken_prevent_dup_job_exec_test

bundle install

docker-compose up -d redis localstack
dc logs -f --tail=10
# Check redis and localstack are started successfully.

bin/rails runner 'HelloWorldJob.perform_later'

bundle exec shoryuken -R -q http://localhost:4566/000000000000/my-test -P ./tmp/pids/shoryuken.pid
# => Hello world!
```

## Test exclusive lock

重複配信を意図して発生させるのは非常に困難なため、今回はロックのキーを固定することで擬似的に再現します。
まず `lib/shoryuken/middleware/server/lock.rb` を下記のように修正します。

```diff
diff --git a/lib/shoryuken/middleware/server/lock.rb b/lib/shoryuken/middleware/server/lock.rb
index 0df0158..4ce8f55 100644
--- a/lib/shoryuken/middleware/server/lock.rb
+++ b/lib/shoryuken/middleware/server/lock.rb
@@ -10,9 +10,9 @@ module Shoryuken::Middleware::Server
       redis = ::Redis.new(url: self.class.redis_url)

       # 本番用
-      lock_key = "shoryuken_lock/#{queue}/#{sqs_msg.message_id}"
+      # lock_key = "shoryuken_lock/#{queue}/#{sqs_msg.message_id}"
       # テスト用
-      # lock_key = "shoryuken_lock/test"
+      lock_key = "shoryuken_lock/test"

       locked = redis.set(lock_key, 'lock', ex: 60, nx: true)
       unless locked
```

そして下記のように操作します。

```
redis-cli set "shoryuken_lock/test" "lock"

redis-cli get "shoryuken_lock/test"
# => lock

bin/rails runner 'HelloWorldJob.perform_later'

bundle exec shoryuken -R -q http://localhost:4566/000000000000/my-test -P ./tmp/pids/shoryuken.pid
# => D, [2020-11-27T15:20:44.824795 #66209] DEBUG -- : Assigning ff0c6216-72cf-8541-271a-6a79e773f4aa
# => I, [2020-11-27T15:20:44.825576 #66209]  INFO -- : started at 2020-11-27 15:20:44 +0900
# :
# => I, [2020-11-27T15:20:44.832421 #66209]  INFO -- : Cannot get lock. lock_key: shoryuken_lock/test

# リトライ中に別ターミナルでロックキーを削除
redis-cli del "shoryuken_lock/test"

# => D, [2020-11-27T15:21:44.819637 #66209] DEBUG -- : Assigning ff0c6216-72cf-8541-271a-6a79e773f4aa
# => I, [2020-11-27T15:21:44.820113 #66209]  INFO -- : started at 2020-11-27 15:21:44 +0900
# :
# => D, [2020-11-27T15:21:44.825613 #66209] DEBUG -- : Locked. lock_key: shoryuken_lock/test
# => Hello world!
# => D, [2020-11-27T15:21:44.898946 #66209] DEBUG -- : Expand lock expiration. lock_key: shoryuken_lock/test
# :
```

ログを見ると分かる通り、Redisにロックキーが存在する間はJobの実行がスキップされます。
リトライまでにロックキーがなくなると、正しく実行されています。
